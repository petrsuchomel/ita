var repository = require('../repository/repository');

class RecordService {
    constructor() {

    }

    getAllRecords(req,callback) {
        console.log("service log - getRecord - " + req.method, req.originalUrl)
        repository.getRecords(callback);
    }

    getRecordById(req, id, callback) {
        console.log("service log - getRecordById - " + req.method, req.originalUrl)
        repository.getRecordsById(id, callback);
    }

        updateRecord(req,id, body, options, callback) {
            console.log("service log - updateRecord - " + req.method, req.originalUrl)
            repository.updateRecordsById(id, body, options, callback);
        }

        addRecord(req,body, callback) {
            console.log("service log - addRecord - " + req.method, req.originalUrl)
            repository.addRecords(body, callback)
    }

}

module.exports = new RecordService();
