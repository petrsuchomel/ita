// const mongoose = require('mongoose');
const recordModel = require('../model/record');

class RecordRepository {
    constructor() {}


    getRecords(callback) {
        const instance = recordModel;
        return instance.find(callback);
    }

    getRecordsById(id, callback) {
        const instance = recordModel;
        return instance.findById(id, callback);
    }

    updateRecordsById(id, body, options, callback) {
      var query = {_id: id};
      // var update = {
      //     name: body.name,
      //     description: body.description,
      //     distance: body.distance
      // }
        const instance = recordModel;
        return instance.findOneAndUpdate(query, body, options, callback);
        // return instance.findOneAndUpdate(query, {$set:body}, options, callback);
    }

    addRecords(body, callback) {
        const instance = recordModel;
        return instance.create(body, callback);
    }


}
module.exports = new RecordRepository();
