const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const recordSchema = new Schema({
    id: {
        type: String,
        // required: true
    },
    type: {
        type: Array
    },
    name: {
        type: String
    },
    description: {
        type: String
    },
    distance: {
        type: Number
    },
    created: {
        type: Date
    },
    updated: {
        type: Date
    },
    duration: {
        type: Number
    }
});

const record = mongoose.model('record', recordSchema);
module.exports = record;
