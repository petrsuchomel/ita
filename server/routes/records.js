const express = require('express');
const router = express.Router();
const RecordService = require('../service/record.service.js');


router.use((req, res, next) => {
    console.log('Time: ', new Date());
    next();
});

// get all records
router.get('/', (req, res) => {
    console.log("I am Routes")
    RecordService.getAllRecords(req, function(err, record) {
        if (err) {
            console.log("http log - getAllRecords - " + res.sendStatus(404) + err);
        }
        res.json(record);
    });
});

// get record by id
router.get('/:id', function(req, res) {
    RecordService.getRecordById(req,req.params._id, function(err, record) {
        if (err) {
            console.log("http log - getRecordById - " + res.sendStatus(404) + err);
        }
        res.json(record);
    });
});

// update record
router.put('/:id', function(req, res) {
    RecordService.updateRecord(req,req.params._id, req.body, {}, function(err, record) {
        if (err) {
            console.log("http log - updateRecord - " + res.sendStatus(500) + err);
        }
        res.json(record);
    });
});

// upload new record
router.post('/', function(req, res) {
    RecordService.addRecord(req,req.body, function(err, record) {
        if (err) {
            console.log("http log - addRecord - " + res.sendStatus(500) + err);
        }
        res.json(record);
    });
});

module.exports = router;
