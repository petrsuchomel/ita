export class RecordModel {
    id: any;
    type: any;
    name: any;
    description: string;
    distance: number;
    created: any;
    updated: any;
    duration: any; // seconds
}
