import {NgModule} from "@angular/core";
import {CommonModule}  from '@angular/common';

import {RecordsComponent} from "./records.component";
import {RecordService} from "./records.service";

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        RecordsComponent
    ],
    exports: [RecordsComponent],

    providers: [RecordService],

    bootstrap: [RecordsComponent]

})
export class RecordsModule {
}
