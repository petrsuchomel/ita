import {Component, ViewEncapsulation} from "@angular/core";
import {RecordService} from "./records.service";
import {RecordModel}from"../models/record.model";


@Component({
    selector: 'my-records',
    templateUrl: 'records.component.html',
    encapsulation: ViewEncapsulation.None,
})
export class RecordsComponent {
    private Records: RecordModel[];

    constructor(private service: RecordService) {
    }

    ngOnInit() {
        this.getHardcodedRecord();
    }


    getHardcodedRecord() {
        this.Records = this.service.getData();
    }




}
